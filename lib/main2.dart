import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contoh Stateful',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Contoh 1'),
        ),
        body: inputanUser(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class inputanUser extends StatefulWidget {
  @override
  _inputanUserState createState() => _inputanUserState();
}

class _inputanUserState extends State<inputanUser> {

  String isiTulisan = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextField(
          onChanged: (text) => sayHello(text),
        ),
        Text(isiTulisan)
      ],
    );
  }

  void sayHello(String isiText) {
    setState(() {
      isiTulisan = 'hello ' + isiText.toUpperCase() + ' 1';
    });
  }
}