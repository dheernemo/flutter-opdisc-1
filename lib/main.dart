import 'package:flutter/material.dart';
import 'main2.dart';
import 'main_whatsapp.dart';

void main() => runApp(MyWhatsapp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aplikasi Pertama',
      theme: ThemeData(
        primarySwatch: Colors.green
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello World'),
        ),
        body: inputanUser(),
      ),
    );
  }
}