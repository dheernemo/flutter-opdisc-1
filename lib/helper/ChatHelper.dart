import 'package:opdics/model/ChatModel.dart';

class ChatHelper {
  
  static var chatList = [
    ChatModel('Aku', 'apa kabar?', '28/04/2019', 'foto/gambar1.png'),
    ChatModel('DIa', 'lagi ngapain?', '28/04/2019', 'foto/gambar1.png'),
    ChatModel('Dia', 'udah makan?', '28/04/2019', 'foto/gambar1.png'),
    ChatModel('Mereka', 'Ping!!', '28/04/2019', 'foto/gambar1.png'),
  ];

  static ChatModel getItem(int position) {
    return chatList[position];
  }

  static var itemCount = chatList.length;

}